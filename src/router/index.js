import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '@/components/LoginPage'
import HomePage from '@/components/HomePage'
import Restaurant from '@/components/Restaurant'
import SignIn from '@/components/SignIn'
import TestAPI from '@/components/TestAPI'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'LoginPage',
      component: LoginPage
    },
    {
      path: '/HomePage',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/Restaurant',
      name: 'Restaurant',
      component: Restaurant
    },
    {
      path: '/SignIn',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/TestAPI',
      name: 'TestAPI',
      component: TestAPI
    }
  ]
})
