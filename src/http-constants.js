import axios from 'axios'

let baseURL

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  baseURL = 'http://jsonplaceholder.typicode.com/todos'
} else {
  baseURL = 'http://jsonplaceholder.typicode.com/todos'
}

export const HTTP = axios.create(
  {
    baseURL: baseURL
  })
